global _start

section .data
message: db 'Hello, World!', 10	; 10 é o valor do caractere '\n' na tabela ASCII 

section .text
_start:
	mov	rax, 1		; O número da syscall deve ser armazenada em RAX: 1 por ser uma escrita (mesma situação da syscall 5 no MIPS para realizar input)
				; Os registradores que armazenam os parâmetros da syscall são, respectivamente, RDI, RSI, RDX, R10, R8 e R9.
	mov	rdi, 1		; #1 argumento: descritor que receberá a escrita (0 - stdin, 1 - stdout, 2 - stderr)
	mov	rsi, message	; #2 argumento: fluxo de bytes que será escrito (ao indicar um rótulo, no caso 'message', o fluxo iniciará no indice 0 da cadeia de bytes do rótulo)
	mov	rdx, 14		; #3 argumento: tamanho da cadeia de bytes que será escrita
	syscall			; Chamada da syscall informada em RAX

	mov	rax, 60		; Sycall 60: exit
	xor	rdi, rdi	; Zerando RDI: o RDI zerado será o primeiro argumento da syscall 60. A execução resultará em um 'exit(0)', informando que tudo ocorreu normalmente.
	syscall
