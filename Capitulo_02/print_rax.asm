section .data
codes:
	db	'0123456789ABCDEF'

section .text
global _start
_start:
	; O número 1122... será escrito em hexa
	mov	rax, 1122334455667788

	mov	rdi, 1
	mov	rdx, 1
	mov	rcx, 64

	; Cada nibble deve ser exibido como um dígito hexadecimal
	; Use o deslocamento e o AND para isolar cada nibble
	; O resultado será o offset no array 'codes'
.loop:
	push	rax
	sub	rcx, 4
	sar	rax, cl			; cl é o nibble mais baixo de RCX (RCX - ECX - CX - CH + CL).
					; SAR é o mnemônico de Shift Arithmetic Right, ele é utilizado para divisões de valores com sinal.
	and	rax, 0xf		; Um AND com 0xf coloca o valor dentro de um intervalo de 0 a 15 e o resultado será o valor do último byte.

	lea	rsi, [codes + rax]	; O resultado do AND é o offset em 'codes', ie, RAX contém o índice de 'codes'.
					; LEA - Load Effective Address: calcula o endereço de uma célula de memória e armazena em um registrador.
					; Em nosso caso calcula codes + RAX, acessa este endereço na memória e escreve em RSI.
	mov	rax, 1

	; syscall alterará RCX e R11
	push	rcx
	syscall
	pop	rcx

	pop	rax
	
	; test pode ser usado para uma verificação mais rápida do tipo 'é um zero?'
	test	rcx, rcx
	jnz .loop

	mov	rax, 60
	xor	rdi, rdi
	syscall
